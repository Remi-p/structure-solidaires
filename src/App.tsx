import { PropsWithChildren, useCallback, useState } from "react";
import "./App.css";
import STRUCTURES from "./assets/Structures.json";

const DataCell = ({
  type,
  children,
  ...rest
}: PropsWithChildren<
  React.HTMLAttributes<HTMLTableCellElement> & {
    type: "th" | "td";
    scope?: "col" | "row";
  }
>) => {
  if (type === "th") return <th {...rest}>{children}</th>;

  return <td {...rest}>{children}</td>;
};

const Cell = ({
  name,
  url,
  img,
  selected,
  selectInProgress,
  lineSelected,
  type,
  ...rest
}: React.HTMLAttributes<HTMLTableCellElement> & {
  name: string;
  url?: string;
  img?: string;
  selected: boolean;
  selectInProgress: boolean;
  lineSelected?: boolean;
  type: "th" | "td";
  scope?: "col" | "row";
}) => {
  const content = (
    <>
      {selected ? <em>{name}</em> : name}
      {img && (
        <>
          <br />
          <img src={img} alt="" />
        </>
      )}
    </>
  );
  const children = url ? <a href={url}>{content}</a> : content;

  if (!selectInProgress)
    return (
      <DataCell type={type} {...rest}>
        {children}
      </DataCell>
    );

  return (
    <DataCell
      className={
        selected ? "selected" : lineSelected ? "line-selected" : "not-selected"
      }
      type={type}
      {...rest}
    >
      {children}
    </DataCell>
  );
};

function App() {
  const [selectInProgress, setSelectInProgress] = useState(true);
  const [ulSelected, setUlSelected] = useState(2);
  const [natioSelected, setNatioSelected] = useState(2);

  const handleMouseEnter = useCallback((national: number, ul: number) => {
    setSelectInProgress(true);
    setUlSelected(ul);
    setNatioSelected(national);
  }, []);
  const handleMouseLeave = useCallback(() => {
    setSelectInProgress(false);
  }, []);

  return (
    <table onMouseLeave={handleMouseLeave}>
      <caption>Organisation de l'Union Syndicale Solidaires</caption>
      <tr>
        <td colSpan={2} className="axis"></td>
        <th colSpan={STRUCTURES[0].length} className="axis">
          Unions locales
        </th>
      </tr>
      <tr>
        <td className="axis"></td>
        {STRUCTURES[0].map(
          (ul, ulIdx) =>
            ul && (
              <Cell
                {...ul}
                selected={ulIdx === ulSelected}
                selectInProgress={selectInProgress}
                type="th"
                scope="col"
              />
            )
        )}
      </tr>
      {STRUCTURES.slice(1).map((national, natioIdx) => (
        <tr>
          {natioIdx === 0 && (
            <th className="axis vertical" rowSpan={STRUCTURES.length}>
              Fédérations / Syndicats professionnels
            </th>
          )}
          {national[0] && (
            <Cell
              {...national[0]}
              selected={natioSelected === natioIdx + 1}
              selectInProgress={selectInProgress}
              type="th"
              scope="row"
            />
          )}
          {national
            .slice(1)
            .map(
              (section, sectionIdx) =>
                section && (
                  <Cell
                    {...section}
                    selected={
                      ulSelected === sectionIdx + 1 &&
                      natioSelected === natioIdx + 1
                    }
                    lineSelected={
                      ulSelected === sectionIdx + 1 ||
                      natioSelected === natioIdx + 1
                    }
                    type="td"
                    selectInProgress={selectInProgress}
                    onMouseEnter={() =>
                      handleMouseEnter(natioIdx + 1, sectionIdx + 1)
                    }
                  />
                )
            )}
        </tr>
      ))}
    </table>
  );
}

export default App;
